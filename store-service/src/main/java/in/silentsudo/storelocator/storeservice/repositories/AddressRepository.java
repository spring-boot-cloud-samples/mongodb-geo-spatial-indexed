package in.silentsudo.storelocator.storeservice.repositories;

import in.silentsudo.storelocator.storeservice.entities.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepository extends MongoRepository<Address, String> {
}
