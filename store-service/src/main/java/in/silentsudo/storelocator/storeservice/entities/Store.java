package in.silentsudo.storelocator.storeservice.entities;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@ToString
public class Store {

    @Getter
    @Id
    private String id;

    @Getter
    private String name;

    @Getter
    private Address address;


    public Store(String name, Address address) {
        id = null;
        this.name = name;
        this.address = address;
    }

    protected Store() {
        id = null;
        this.name = null;
        this.address = null;
    }

}
