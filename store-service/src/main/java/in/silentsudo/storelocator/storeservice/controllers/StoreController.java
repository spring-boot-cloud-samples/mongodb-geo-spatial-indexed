package in.silentsudo.storelocator.storeservice.controllers;

import in.silentsudo.storelocator.storeservice.entities.Store;
import in.silentsudo.storelocator.storeservice.services.StoreService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/store-finder")
public class StoreController {

    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping
    public Page<Store> storeFinder(@RequestParam("lat") Double myLat,
                                   @RequestParam("long") Double myLong,
                                   @RequestParam("radius") Integer radiusInKm,
                                   @RequestParam("page") int page,
                                   @RequestParam("size") int size) {
        return storeService.getStoresNearMe(myLat, myLong, radiusInKm, PageRequest.of(page, size));
    }

    @GetMapping("/all")
    public List<Store> all() {
        return storeService.all();
    }

}
