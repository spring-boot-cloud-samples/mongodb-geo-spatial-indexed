package in.silentsudo.storelocator.storeservice.repositories;

import in.silentsudo.storelocator.storeservice.entities.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends MongoRepository<Store, String> {

    Store findByAddressLocationXAndAddressLocationY(Double x, Double y);

    Page<Store> findByAddressLocationNear(Point point, Pageable pageable);
}
