package in.silentsudo.storelocator.storeservice.services;

import in.silentsudo.storelocator.storeservice.entities.Store;
import in.silentsudo.storelocator.storeservice.repositories.StoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class StoreService {
    private final StoreInitializer storeInitializer;
    private final StoreRepository storeRepository;

    public StoreService(StoreInitializer storeInitializer, StoreRepository storeRepository) {
        this.storeInitializer = storeInitializer;
        this.storeRepository = storeRepository;
    }

    public Page<Store> getStoresNearMe(Double myLat, Double myLong,
                                       Integer radiusInKm,
                                       PageRequest pageRequest) {
        return storeRepository.findByAddressLocationNear(
                new Point(myLat, myLong), pageRequest
        );
    }

    public List<Store> all() {
        try {
            return storeInitializer.readStores();
        } catch (Exception e) {
            log.error("Exception getting all stores", e);
            return Collections.emptyList();
        }
    }
}
