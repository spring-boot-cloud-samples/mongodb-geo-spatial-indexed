package in.silentsudo.storelocator.storeservice.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;


@AllArgsConstructor
@NoArgsConstructor
@Document
@ToString
public class Address {

    @Getter
    private String street;

    @Getter
    private String city;

    @Getter
    private String zip;

    /**
     * Creat index like
     * db.store.createIndex({"address.location": "2d"});
     * in mongo db to make it work wise we get exception
     */
    @Getter
    private @GeoSpatialIndexed
    Point location;
}
