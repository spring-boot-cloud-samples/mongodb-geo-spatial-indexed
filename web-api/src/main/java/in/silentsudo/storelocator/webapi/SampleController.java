package in.silentsudo.storelocator.webapi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SampleController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(value = "name", defaultValue = "Greeting!") String name, Model model) {
        model.addAttribute("greeting", name);
        return "greetings";
    }

}
