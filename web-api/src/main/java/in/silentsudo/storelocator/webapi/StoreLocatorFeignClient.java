package in.silentsudo.storelocator.webapi;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "store-locator-service")
public interface StoreLocatorFeignClient {

}
